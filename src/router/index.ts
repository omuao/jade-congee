import { createRouter, createWebHistory } from 'vue-router'
import StageView from '@/views/StageView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'stage',
      component: StageView
    }
  ]
})

export default router
